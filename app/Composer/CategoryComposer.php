<?php


namespace App\Composer;


use App\Models\Category;
use Illuminate\View\View;

class CategoryComposer
{
    protected $category;


    public function __construct(Category $category)
    {
        $this->category = $category;
    }


    public function compose(View $view)
    {
        $categories = $this->category->whereNull('parent_id')->get(['id','name']);
        $view->with(['categories'=>$categories]);
    }


}
