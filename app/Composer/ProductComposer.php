<?php


namespace App\Composer;


use App\Models\Product;
use Illuminate\View\View;

class ProductComposer
{

    protected $product;


    public function __construct(Product $product)
    {
        $this->product = $product;
    }


    public function compose(View $view)
    {

        $products = $this->product->get(['id','name','price','image']);
        $view->with(['products' => $products]);
    }


}
