<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HandleImageTrait;
use Illuminate\Notifications\Notifiable;
class Product extends Model
{
    use Notifiable;
    use HandleImageTrait;
    protected $table = 'products';


    protected $fillable = [
        'name', 'price', 'sale','image'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product', 'product_id', 'category_id');
    }

    public function assignCategories($categoryIds = [])
    {
        return $this->categories()->sync($categoryIds);
    }

    public function addCategories($categoryIds)
    {
        return $this->categories()->attach($categoryIds);
    }

    public function removeCategories($categoryIds)
    {
        return  $this->categories()->detach($categoryIds);
    }


    public function getProductHasCategory($categoryName){

        return $this->whereHas('categories', function ($query) use ($categoryName){
            $query->where('name', $categoryName);
        })
            ->get();
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name','LIKE','%'.$name.'%') :  null;
    }

    public function scopeWithCategoryId($query, $categoryId)
    {
        return $categoryId ? $query->whereHas('categories', function ($q) use ($categoryId) {
            return $q->where('categories.id', $categoryId);
        })
            :  null;
    }

}
