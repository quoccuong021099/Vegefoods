<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Coupon extends Model
{
    use Notifiable;

    protected $table = 'coupons';

    protected $fillable = [
        'code', 'expiry_date', 'quantity', 'type', 'value',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
