<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'user_id',
        'customer',
        'address',
        'phone',
        'payment',
        'ship',
        'coupon_id',
        'status',
        'total',
        'discount',
        'order_create',
    ];

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class, 'order_id');
    }

}
