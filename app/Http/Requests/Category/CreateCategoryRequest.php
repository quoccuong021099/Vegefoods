<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' =>'required',
            'parent_id'=>'required',


        ];
    }

    public function messages()
    {
        return [
            'name.required' =>'name không được để trống',
            'parent_id.required' =>'parent_id không được để trống',


        ];
    }
}
