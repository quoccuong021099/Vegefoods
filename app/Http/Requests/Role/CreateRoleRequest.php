<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;

class CreateRoleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'name'=>'required|unique:roles',
            'display_name'=>'required',


        ];
    }

    public function messages()
    {
        return [
            'name.required' =>'name không được để trống',
            'display_name.required' =>'display_name không được để trống',


        ];
    }
}
