<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name'=>'required',
            'sale'=>'required',
            'image' =>'nullable|mimes:jpg,bmp,png|file|image',
            'price'=>'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required' =>'Tên không được để trống',
            'sale.required' =>'Sale không được để trống',
            'image.required' =>'Image không được để trống',
            'price.required' =>'Price không được để trống',

        ];
    }
}
