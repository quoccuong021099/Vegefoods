<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required',
            'sale'=>'required',
            'image' =>'nullable|mimes:jpg,bmp,png|file|image',
            'price'=>'required',
        ];
    }
}
