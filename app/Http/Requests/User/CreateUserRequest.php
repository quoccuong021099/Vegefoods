<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'required',
            'phone'=>'required',
            'address'=>'required',
            'email'=>'required|email|unique:users,email',
            'password' =>'min:6',
            'image' =>'nullable|mimes:jpg,bmp,png|file|image'
        ];
    }

    public function messages()
    {
        return [
            'name.required' =>'Tên không được để trống',
            'phone.required' =>'Phone không được để trống',
            'email.required' =>'Email không được để trống',
            'address.required' =>'Email không được để trống',
            'image.required' =>'Image không được để trống',
        ];
    }
}
