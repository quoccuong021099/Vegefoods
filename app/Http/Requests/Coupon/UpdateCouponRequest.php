<?php

namespace App\Http\Requests\Coupon;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCouponRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'code'=>'required|unique:coupons,code,'.$this->id,
            'type'=>'required',
            'value'=>'required|numeric|min:0',
            'status'=>'required',
            'expiry_date'=>'required',
            'quantity'=>'required|numeric|min:0',

        ];
    }
}
