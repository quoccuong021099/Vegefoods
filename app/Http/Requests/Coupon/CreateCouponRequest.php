<?php

namespace App\Http\Requests\Coupon;

use Illuminate\Foundation\Http\FormRequest;

class CreateCouponRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'code'=>'required|unique:coupons,code',
            'type'=>'required',
            'value'=>'required|numeric|min:0',
            'status'=>'required',
            'expiry_date'=>'required',
            'quantity'=>'required|numeric|min:0',

        ];
    }

    public function messages()
    {
        return [
            'code.required' =>'code không được để trống',
            'expiry_date.required' =>'expery_date không được để trống',
            'quantity.required' =>'quantity không được để trống',
            'value.required' =>'value không được để trống',
            'status.required' =>'max_value không được để trống',
            'type.required' =>'type không được để trống',

        ];
    }
}
