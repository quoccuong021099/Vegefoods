<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Traits\HandleImageTrait;
use App\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    use HandleImageTrait;

    protected $path;
    protected $roleModel;

    protected $userModel;

    public function __construct(User $user, Role $role)
    {
        $this->userModel = $user;
        $this->path = 'upload/';
        $this->roleModel = $role;
    }



    public function index()
    {

        $users = $this->userModel->with('roles')->latest('id')->paginate(5);

        return view('manager.user.index')->with( 'users',$users);

    }


    public function create()
    {
        $roles = $this->roleModel->where('name', '!=', 'super-admin')->get();
        return view('manager.user.create', compact('roles'));
    }


    public function store(CreateUserRequest $request)
    {

        $image = $request->file('image');

        $dataCreate = $request->all();
        $dataCreate['image'] = $this->saveImage($image, $this->path);
        $user =$this->userModel->create($dataCreate);
        $user->syncRoles($request->roles);
        return redirect()->route('users.index')->with('message', 'Create user success');
    }


    public function show($id)
    {
        //
    }



    public function edit($id)
    {
        $user = $this->userModel->with('roles')->findOrFail($id);

        $roles = $this->roleModel->where('name', '!=', 'super-admin')->get();
        $listRoleIds = $user->roles->pluck('id')->toArray();
        return view('manager.user.edit')->with(['user'=> $user, 'roles' =>$roles, 'listRoleIds' =>$listRoleIds]);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->userModel->findOrFail($id);
        $image = $request->file('image');

        $dataUpdate = $request->all();

        $dataUpdate['image'] = $this->updateImage($image, $this->path, $user->image);

        $user->update($dataUpdate);
        $user->syncRoles($request->roles);
        return redirect()->route('users.index')->with('message', 'Edit user success');

    }


    public function destroy($id)
    {
        $users = $this->userModel->findOrFail($id);
        $users->delete();

        //Thực hiện chuyển trang        //Thực hiện chuyển trang
        return redirect()->route('users.index')->with('message', 'Delete user success');
    }
}
