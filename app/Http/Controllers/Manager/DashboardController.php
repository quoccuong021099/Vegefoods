<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Product;
use App\Traits\HandleImageTrait;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    use HandleImageTrait;

    protected $categoryModel;
    protected $productModel;
    protected $userModel;
    protected $couponModel;
    protected $orderModel;


    public function __construct(Product $product,Category $categories, User $user, Coupon $coupons, Order $orders)
    {

        $this->categoryModel = $categories;
        $this->productModel = $product;
        $this->userModel = $user;
        $this->couponModel = $coupons;
        $this->orderModel = $orders;

    }

    public function index()
    {


        $products =  $this->productModel->count();
        $categories = $this->categoryModel->count();
        $user = $this->userModel->count();
        $coupons = $this->couponModel->count();
        $orders = $this->orderModel->count();

        return view('manager.dashboard.index', compact('products','user','categories','coupons','orders'));

    }

}
