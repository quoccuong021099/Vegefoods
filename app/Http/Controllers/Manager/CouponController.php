<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\Coupon\CreateCouponRequest;
use App\Http\Requests\Coupon\UpdateCouponRequest;
use App\Traits\HandleImageTrait;
use App\Models\User;
use App\Models\Coupon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class CouponController extends Controller
{
    public function index()
    {
        $coupons = Coupon::paginate(3);

        return view('manager.coupon.index')->with( 'coupons',$coupons);
    }


    public function create()
    {
        return view('manager.coupon.create');

    }

    public function store(Request  $request)
    {

        $dataCreate = $request->all();

        Coupon::create($dataCreate);

        return redirect()->route('coupon.index')->with('message', 'Create coupon success');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $coupon = Coupon::findOrFail($id);

        return view('manager.coupon.edit')->with('coupon', $coupon);
    }


    public function update(Request  $request, $id)
    {
        $coupon = Coupon::findOrFail($id);


        $dataUpdate = $request->all();


        $coupon->update($dataUpdate);

        return redirect()->route('coupon.index')->with('message', 'Edit coupon success');
    }

    public function destroy($id)
    {
        $coupon = Coupon::findOrFail($id);
        $coupon->delete();

        //Thực hiện chuyển trang
        return redirect()->route('coupon.index')->with('message', 'Delete coupon success');
    }
}
