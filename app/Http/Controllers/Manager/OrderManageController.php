<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderManageController extends Controller
{
    protected  $categoryModel;
    protected  $productModel;
    protected  $cart;
    protected  $coupon;
    protected  $order;
    protected  $orderDetail;


    public function __construct(Category $categoryModel,Product $product, Cart $cart, Coupon $coupon, Order $order, OrderDetail $orderDetail)
    {
        $this->categoryModel = $categoryModel;
        $this->productModel = $product;
        $this->cart = $cart;
        $this->coupon = $coupon;
        $this->orderDetail = $orderDetail;
        $this->order = $order;
    }

    public function index(Request $request)
    {
        $orders = $this->order->with(['orderDetails'])->latest('id')->paginate(10);

        return view('manager.order.index', compact('orders'));
    }

    public function show($id)
    {
        $order = $this->order->with(['orderDetails', 'orderDetails.product'])->findOrFail($id);

        return view('manager.order.show', compact('order'));
    }
}
