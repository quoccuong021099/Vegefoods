<?php

namespace App\Http\Controllers\Client;


use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    protected $categoryModel;
    protected $productModel;
    protected $cart;
    protected $coupon;
    protected $order;
    protected $orderDetail;


    public function __construct(Category $categoryModel,Product $product, Cart $cart, Coupon $coupon, Order $order, OrderDetail $orderDetail)
    {
        $this->categoryModel = $categoryModel;
        $this->productModel = $product;
        $this->cart = $cart;
        $this->coupon = $coupon;
        $this->orderDetail = $orderDetail;
        $this->order = $order;
    }


    public function index()
    {
        $userId = auth()->check() ? auth()->user()->id : 1;

        $carts = $this->cart->with(['user', 'product'])->where('user_id',$userId)->get();

        return view('user.order.index', compact('carts'));
    }

    public function addCouponToOrder(Request  $request)

    {


        $code = strtoupper($request->code);


        $coupon = $this->coupon->where('code', $code)->first();

        $discount = 0;

        if($coupon)
        {
            $userId = auth()->check() ? auth()->user()->id : 1;
            $carts = $this->cart->with(['user', 'product'])->where('user_id',$userId)->get();
            $total = $carts->sum('total');


            if ($coupon->type == 'Money') {
                $discount = $coupon->value;
            }
            switch ($coupon->type) {
                case 'Money':
                    $discount = $coupon->value;
                    break;
                case 'Percent':
                    $discount = round(($total * $coupon->value) / 100, 2);
                    break;
                case 'Freeship':
                    $discount = 10;
                    break;

            }
            session(['discount'=> $discount]);
            session(['totalAfterDiscount'=>$total-$discount]);
            session(['code' => $coupon->code]);

            return redirect()->route('orders.index')->with('message' , 'apply code success');


        }else{
            return redirect()->route('orders.index')->with('message' , 'Code not found');
        }
    }


    public function store(Request  $request)
    {

        $data = $request->all();
        $data['user_id'] = auth()->check() ? auth()->user()->id : 1;

        $carts = $this->cart->with(['product'])->where('user_id',    $data['user_id'])->get();
        $coupon=0.0;
        if (session('code'))
        {
            $coupon = $this->coupon->where('code', session('code'))->first();
                $data['coupon_id'] = $coupon->id;
        }



        $data['ship'] = 'GHTK';
        $data['order_create'] = now();
        $data['total'] = $carts->sum('total');
        $data['discount'] =session('discount');
        if($data['discount'] == null)
        {
            $data['discount'] = 0.0;
        }
        $data['status'] = 'tiep nhan don hang';



        $order = $this->order->create($data);

        foreach ($carts as $item)
        {
            $dataDetail['order_id'] = $order->id;
            $dataDetail['product_id'] = $item->product->id;
            $dataDetail['size'] = 20;
            $dataDetail['quantity'] = $item->quantity;
            $dataDetail['color'] = 'do';

            $this->orderDetail->create($dataDetail);
            $item->delete();
        }
        session()->forget(['code', 'discount', 'totalAfterDiscount']);

        if (session('code'))
        {
            $quantity =  $coupon->quantity -1;

            $coupon->update(['quantity' =>$quantity]);
        }
        return redirect()->route('orders.list');
    }


    public function list()
    {
        $userId = auth()->check() ? auth()->user()->id : 1;

        $orders = $this->order->with(['orderDetails', 'orderDetails.product' ])->where('user_id', $userId)->get();

        return view('user.order.list', compact('orders'));

    }


}
