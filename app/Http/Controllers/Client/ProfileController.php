<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\Role;
use App\Traits\HandleImageTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    use HandleImageTrait;

    protected $path;
    protected $roleModel;

    protected $userModel;

    public function __construct(User $user, Role $role)
    {
        $this->userModel = $user;
        $this->path = 'upload/';
        $this->roleModel = $role;
    }


    public function index()
    {

        $users = $this->userModel;

        return view('user.profile.index')->with( 'users',$users);
    }

    public function edit($id)
    {
        $user = $this->userModel->with('roles')->findOrFail($id);

        $roles = $this->roleModel->where('name', '!=', 'super-admin')->get();
        $listRoleIds = $user->roles->pluck('id')->toArray();
        return view('user.profile.edit')->with(['user'=> $user, 'roles' =>$roles, 'listRoleIds' =>$listRoleIds]);
    }


    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->userModel->findOrFail($id);
        $image = $request->file('image');

        $dataUpdate = $request->all();

        $dataUpdate['image'] = $this->updateImage($image, $this->path, $user->image);

        $user->update($dataUpdate);
        $user->syncRoles($request->roles);
        return redirect()->route('profile.index')->with('message', 'Edit user success');

    }
}
