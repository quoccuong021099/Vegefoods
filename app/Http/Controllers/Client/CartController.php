<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    protected $categoryModel;
    protected $productModel;
    protected $cart;

    /**
     * CategoryController constructor.
     * @param Category $categoryModel
     * @param Product $product
     * @param Cart $cart
     */
    public function __construct(Category $categoryModel,Product $product, Cart $cart)
    {
        $this->categoryModel = $categoryModel;
        $this->productModel = $product;
        $this->cart = $cart;
    }


    public function index()
    {

        $userId = auth()->check() ? auth()->user()->id : 1;

        $carts = $this->cart->with(['user', 'product'])->where('user_id',$userId)->get();

        return view('user.cart.index', compact('carts'));

    }
    public function addToCart(Request $request)
    {
        $data['product_id'] = $request->product_id;
        $data['user_id'] = auth()->check() ? auth()->user()->id : 1;

        $cart = $this->cart->getCartBy($data);

        if ($cart) {
            $quantity = $cart->quantity + 1;
            $cart->update(['quantity' => $quantity]);
        } else {
            $data['quantity'] = 1;
            $cart = $this->cart->create($data);
        }
        $cartTotal = $this->cart->where('user_id', auth()->check() ? auth()->user()->id : 1)->count();

        return response()->json([
            'status' => 200,
            'message' => ' add To Cart Success',
            'cart_total' => $cartTotal
        ]);
    }


    public function removeToCart(Request $request)
    {

    }
    public function changeQuantityCart(Request $request)
    {
        $data['product_id'] = $request->product_id;
        $data['user_id'] = auth()->check() ? auth()->user()->id : 1;

        $cart = $this->cart->getCartBy($data);

        $quantity = $cart->quantity + $request->quantity;

        $cart->update(['quantity' => $quantity]);

        if($cart->quantity <= 0)
        {
            $cart->delete();
        }

        return response()->json([
            'status' => 200,
            'message' => ' add To Cart Success',
            'data' => $cart
        ]);

    }

}
