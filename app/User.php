<?php

namespace App;

use App\Traits\HandleImageTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use HandleImageTrait;


    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'phone', 'address', 'gender', 'image'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = Hash::make($value);
    }

}
