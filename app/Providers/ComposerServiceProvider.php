<?php

namespace App\Providers;

use App\Composer\CartComposer;
use App\Composer\CategoryComposer;
use App\Composer\ProductComposer;
use App\Composer\UserComposer;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    public function register()
    {

    }

    public function boot()
    {
        view()->composer([
            'user.layout.app'
        ],

            CartComposer::class
        );
        view()->composer([
            'user.layout.navbar'
        ],
            CartComposer::class
        );
//index
        view()->composer([
            'user.home.index'
        ],
            CategoryComposer::class
        );

//show-product
        view()->composer([
            'user.home.product'
        ],
            CategoryComposer::class
        );

//cart
        view()->composer([
            'user.cart.index'
        ],
            CategoryComposer::class
        );

        view()->composer([
            'user.cart.index'
        ],
            CartComposer::class
        );


        view()->composer([
            'manager.product.index'
        ],
            CategoryComposer::class
        );


    }
}
