<?php

use Illuminate\Support\Facades\Route;




Route::group(['middleware'=>'auth'],function(){


Route::group(['prefix'=>'manager', 'namespace'=>'Manager'],function(){


    Route::resource('dashboard', 'DashboardController');
    Route::resource('users', 'UserController');

    Route::resource('category', 'CategoryController');
    Route::resource('product', 'ProductController');
    Route::resource('coupon', 'CouponController');
    Route::resource('roles', 'RoleController');

    Route::get('orders', 'OrderManageController@index')->name('manager.orders.index');
    Route::get('orders/{id}', 'OrderManageController@show')->name('manager.orders.show');


});

//client

    // Route::resource('cart', 'CartController');

    // Route::resource('checkout', 'CartController');



});
//


Route::group(['namespace'=>'Client'],function(){

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/products/{id}', 'HomeController@showProduct')->name('client.show_product');

    Route::get('/carts', 'CartController@index')->name('cart.index');

    Route::post('/add-to-cart', 'CartController@addToCart')->name('cart.add_to');

    Route::post('/change-to-cart', 'CartController@changeQuantityCart')->name('cart.change_quantity');

    Route::get('/orders', 'OrderController@index')->name('orders.index');


    Route::post('/add-coupon-order', 'OrderController@addCouponToOrder')->name('orders.add_coupon');


    Route::post('/orders', 'OrderController@store')->name('orders.store');

    Route::get('/order-list', 'OrderController@list')->name('orders.list');


    Route::get('/contact', 'ContactController@index')->name('contact');

    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::get('/profile-edit', 'ProfileController@edit')->name('profile.edit');
    Route::get('/profile-update', 'ProfileController@update')->name('profile.update');

});

Auth::routes();

