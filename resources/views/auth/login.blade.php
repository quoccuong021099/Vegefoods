@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="wrap-login100">
                <div
                    class="login100-form-title"
                    style="background-image: url({{asset('manager/assetPublic/images/bg-2.jpg')}});"
                >
                    <span class="login100-form-title-1" >Welcome To Vegefoods <br><br> Login </span>
                </div>

                <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">

                    @csrf

                    <div
                        class="wrap-input100 validate-input m-b-26"
                    >
                        <span class="label-input100">{{ __('E-Mail ') }}</span>
                        <input
                            id="email"
                            type="email"
                            class="input100 "
                            name="email"
                            placeholder="Enter email address"
                            required
                            autocomplete="email"
                            autofocus
                            required
                        />
                        <span class="focus-input100"></span>
                    </div>

                    <div
                        class="wrap-input100 validate-input m-b-18"
                    >
                        <span for="password" class="label-input100">{{ __('Password') }}</span>
                        <input
                            class="input100 @error('password') is-invalid @enderror"
                            type="password"
                            placeholder="Enter password"
                            name="password"
                            required
                            autocomplete="current-password"

                        />

                        <span class="focus-input100"></span>
                    </div>
                    <div class="container-login100-form-btn">

                        <div style="margin-bottom: 20px!important;">
                            <span>You don't have an account?</span>
                            <a href="{{ route('register') }}" class="text-primary"  > Register now!</a>
                        </div>
                        <div >
                            <button type="submit" class="btn btn-primary text-center" style="width: 150px" >Login</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
