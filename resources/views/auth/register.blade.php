@extends('layouts.app')

@section('content')
    < <div class="container">
        <div class="row justify-content-center">
            <div class="wrap-login100">
                <div
                    class="login100-form-title"
                    style="background-image: url({{asset('manager/assetPublic/images/bg-2.jpg')}});"
                >
                    <span class="login100-form-title-1"> Welcome To Flower Shop <br><br>Register </span>
                </div>

                <form class="login100-form validate-form" method="POST" action="{{ route('register') }}">

                    @csrf

                    <div class="wrap-input100 validate-input m-b-26" >
                        <span class="label-input100">{{ __('Name') }}</span>
                        <input
                            id="name"
                            type="text"
                            class="input100 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                        />
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                        <span class="focus-input100"></span>
                    </div>




                    <div class="wrap-input100 validate-input m-b-26" >
                        <span class="label-input100">{{ __('Address') }}</span>
                        <input id="address" type="text"
                               class="input100 @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus />
                        @error('address')
                        <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                        <span class="focus-input100"></span>
                    </div>


                    <div class="wrap-input100 validate-input m-b-26" >
                        <span class="label-input100">{{ __('Phone') }}</span>
                        <input id="phone"
                               type="text"
                               class="input100 @error('phone') is-invalid @enderror"
                               name="phone"
                               value="{{ old('phone') }}"
                               required
                               autocomplete="phone"
                               autofocus/>
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                        <span class="focus-input100"></span>
                    </div>


                    <div class="wrap-input100 validate-input m-b-26">
                        <label for="gender" class="label-input100">{{ __('Gender') }}</label>
                        <select
                            style="border: none; outline:none "
                            id="gender"
                            type="text"
                            class="input100 @error('gender') is-invalid @enderror"
                            name="gender"
                            value="{{ old('gender') }}"
                            required
                            autocomplete="gender"
                        >
                            <option value="1">Male</option>
                            <option value="0">Female</option>
                        </select>
                        @error('gender')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>



                    <div class="wrap-input100 validate-input m-b-26" >
                        <span class="label-input100">{{ __('E-Mail ') }}</span>
                        <input
                            id="email"
                            type="email"
                            class="input100 @error('email') is-invalid @enderror"
                            name="email"
                            value="{{ old('email') }}"
                            required
                            autocomplete="email"
                        />
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-18" >
                        <span for="password" class="label-input100">{{ __('Password') }}</span>
                        <input
                            class="input100 @error('password') is-invalid @enderror"
                            name="password"
                            type="password"
                            required
                            autocomplete="new-password"/>

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-18" >
                        <span for="password" class="label-input100">{{ __('Confirm Password') }}</span>
                        <input id="password-confirm"
                               type="password"
                               class="input100"
                               name="password_confirmation"
                               required
                               autocomplete="new-password"/>
                    </div>


                    <div class="container-login100-form-btn">
                        <div style="margin-bottom: 20px!important;">
                        <span>You have an account?</span>
                        <a href="{{ route('login') }}" class="text-primary"  > Login now!</a>
                        </div>
                        <div >
                             <button type="submit" class="btn btn-primary text-center" style="width: 150px;" >{{ __('Register') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
