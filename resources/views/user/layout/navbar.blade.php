<div class="py-1 bg-primary">
    <div class="container">
        <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
            <div class="col-lg-12 d-block">
                <div class="row d-flex">
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><span
                                class="icon-phone2"></span></div>
                        <span class="text">+84 695 04 210</span>
                    </div>
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><span
                                class="icon-paper-plane"></span></div>
                        <span class="text">vegefood@email.com</span>
                    </div>
                    <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
                        <span class="text">3-5 Business days delivery &amp; Free Returns</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar"
     style="font-size: 18px!important;">
    <div class="container">
        <a class="navbar-brand" href="{{route('home')}}">Vegefoods</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="{{route('home')}}" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="{{route('contact')}}" class="nav-link">Contact</a></li>
                <li class="nav-item"><a href="{{route('orders.list')}}" class="nav-link">Checkout</a></li>
                <li class="nav-item cta cta-colored"><a href="{{route('cart.index')}}" class="nav-link">
                        @if(!empty(auth()->user()->id))
                        <span class="icon-shopping_cart notify"  id="cart-total-navbar">[{{$cartTotal}}]</span></a></li>
                @else
                        <span class="icon-shopping_cart notify"  id="cart-total-navbar">[0]</span></a></li>
                {{--                <li class="nav-item cta cta-colored"><a href="cart.html" class="nav-link"><span class="icon-shopping_cart"></span>[0]</a></li>--}}
            @endif
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}"><i class="fa fa-user"></i>{{ __('Login') }}</a>
                    </li>

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}"> <i
                                    class="fa fa-user-plus"></i> {{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    @if(Auth::user()->email == 'cuong021099@gmail.com')
                        <li class="nav-item ">
                            <a href="{{ route('dashboard.index') }}" class="nav-link"><i class="fa fa-tasks"></i>
                                Manage</a>
                        </li>
                    @endif
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('profile')}}">
                            {{-- <i class="icon-user"></i> --}}
                            <img src="{{asset('upload/'.auth()->user() ->image)}}" alt="..."
                            style="width: 20px; height: 20px; border-radius: 100%; ">
                            {{ Auth::user()->name }}
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i>
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->
{{-- <script>

    $(() => {
        $(document).on('click', 'button.deletebtn', function () {
            $(this).closest('tr').remove();
            return false;
        });
        $(document).on('click', '.change-quantity', function (event) {
            event.preventDefault();

            let product_id = $(this).data('product_id');
            let url = $(this).data('action');
            // let quantity = $(this).data('quantity');
            let quantity = $(this).data('quantity');

            $.ajax({
                url: url,
                data: {
                    product_id,
                    quantity
                },
                method: 'post'
            }).then((response) => {
                let {data} = response;
                if (data.quantity <= 0) {
                    $(`#cart-box-${data.id}`).remove();
                } else {
                    $(`#quantity${data.id}`).text(data.quantity)
                    $(`#total${data.id}`).text(+data.quantity * (+data.product.price))
                }
            })
                .catch(error => {

                })
        });


    })



</script> --}}
