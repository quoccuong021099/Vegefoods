<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
<script src="{{ asset('manager/asset/js/application.js') }}" type="text/javascript"></script>
<script src="{{asset('user/asset/js/jquery.min.js')}}"></script>
<script src="{{asset('user/asset/js/jquery-migrate-3.0.1.min.js')}}"></script>
<script src="{{asset('user/asset/js/popper.min.js')}}"></script>
<script src="{{asset('user/asset/js/bootstrap.min.js')}}"></script>
<script src="{{asset('user/asset/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('user/asset/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('user/asset/js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('user/asset/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('user/asset/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('user/asset/js/aos.js')}}"></script>
<script src="{{asset('user/asset/js/jquery.animateNumber.min.js')}}"></script>
<script src="{{asset('user/asset/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('user/asset/js/scrollax.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="{{asset('user/asset/js/google-map.js')}}"></script>
<script src="{{asset('user/asset/js/main.js')}}"></script>
<!-- Forms Validations Plugin -->
<script src="{{ asset('manager/asset/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/jquery.bootstrap-wizard.js') }}"></script>
<script src="{{ asset('manager/asset/js/bootstrap-selectpicker.js') }}"></script>
<script src="{{ asset('manager/asset/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/bootstrap-tagsinput.js') }}"></script>
<script src="{{ asset('manager/asset/js/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/jquery-jvectormap.js') }}"></script>
<script src="{{ asset('manager/asset/js/nouislider.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/core.js') }}"></script>
<script src="{{ asset('manager/asset/js/arrive.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/chartist.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/bootstrap-notify.js') }}"></script>
<script src="{{ asset('manager/asset/js/material-dashboard.js') }}" type="text/javascript"></script>
<script src="{{ asset('manager/asset/demo/demo.js') }}" type="text/javascript"></script>
<script src="{{ asset('manager/asset/js/application.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function(){

        var quantitiy=0;
        $('.quantity-right-plus').click(function(e){
            e.preventDefault();
            var quantity = parseInt($('#quantity').val());
            $('#quantity').val(quantity + 1);
        });

        $('.quantity-left-minus').click(function(e){
            e.preventDefault();
            var quantity = parseInt($('#quantity').val());
            if(quantity>0){
                $('#quantity').val(quantity - 1);
            }
        });

    });
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function () {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts()

        md.initVectorMap()

        md.initFormExtendedDatetimepickers();
        if ($('.slider').length != 0) {
            md.initSliders();
        }

        $('[data-toggle="popover"]').popover()

        $(document).ready(function () {
            // Initialise the wizard
            demo.initMaterialWizard()
            setTimeout(function () {
                $('.card.card-wizard').addClass('active')
            }, 600)
        })
    })


    $(() => {

        $(document).on('click', '.change-quantity', function (event) {
            event.preventDefault();

            let product_id = $(this).data('product_id');
            let url = $(this).data('action');
            // let quantity = $(this).data('quantity');
            let quantity = $(this).data('quantity');

            $.ajax({
                url: url,
                data: {
                    product_id,
                    quantity
                },
                method: 'post'
            }).then((response) => {
                let {data} = response;
                if (data.quantity <= 0) {
                    $(`#cart-box-${data.id}`).remove();
                } else {
                    $(`#quantity${data.id}`).text(data.quantity)
                    $(`#total${data.id}`).text(+data.quantity * (+data.product.price))
                }
            })
                .catch(error => {

                })
        });


    })


</script>
@yield('script')
