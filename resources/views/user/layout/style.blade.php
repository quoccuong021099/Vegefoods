<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
{{--<link href="{{ asset('manager/asset/css/material-dashboard.css') }}" rel="stylesheet"/>--}}

<link rel="stylesheet" href="{{asset('user/asset/css/open-iconic-bootstrap.min.css')}} ">
<link rel="stylesheet" href="{{asset('user/asset/css/animate.css')}}">

<link rel="stylesheet" href="{{asset('user/asset/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('user/asset/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{asset('user/asset/css/magnific-popup.css')}}">

<link rel="stylesheet" href="{{asset('user/asset/css/aos.css')}}">

<link rel="stylesheet" href="{{asset('user/asset/css/ionicons.min.css')}}">

<link rel="stylesheet" href="{{asset('user/asset/css/bootstrap-datepicker.css')}}">
<link rel="stylesheet" href="{{asset('user/asset/css/jquery.timepicker.css')}}">


<link rel="stylesheet" href="{{asset('user/asset/css/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('user/asset/css/icomoon.css')}}">
<link rel="stylesheet" href="{{asset('user/asset/css/style.css')}}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
@yield('style')
