@extends('user.layout.app')
@section('title','Cart')
@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('{{asset('user/asset/images/bg_1.jpg')}}');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center fadeInUp ftco-animated">
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Home</a></span> <span>Cart</span></p>
                    <h1 class="mb-0 bread">My Cart</h1>
                </div>
            </div>
        </div>
    </div>
    @if($carts->count() < 1)
        <h2 class="text-center text-primary" style="margin: 300px 0px;">You have nothing</h2>

    @else
        <section class="ftco-section ftco-cart">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ftco-animate fadeInUp ftco-animated">
                        <div class="cart-list">
                            <table class="table">
                                <thead class="thead-primary">
                                <tr class="text-center">
                                    <th>Remove Product</th>
                                    <th>Images</th>
                                    <th>Product name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($carts as $item)
                                    <tr id="cart-box-{{$item->id}}">
                                        <td><button class="btn btn-success change-quantity"
                                                    data-quantity="-1"
                                                    data-product_id="{{$item->product->id}}"
                                                    data-action="{{route('cart.change_quantity')}}"
                                                    style="margin-right: 10px;width: 30px!important; height: 30px!important;">
                                                <span class="text-white">x</span></button></td>
{{--                                        <td class="td-actions">--}}
{{--                                            <button type="button" data-placement="left" title=""--}}
{{--                                                    class="btn btn-link deletebtn" data-original-title="Remove item">--}}
{{--                                                <i class="material-icons">close</i>--}}
{{--                                            </button>--}}
{{--                                        </td>--}}
                                        <td class="td-name">
                                            <img src="{{asset('upload/'.$item->product->image)}}" alt="..."
                                                 style="width: 80px; ">

                                        </td>
                                        <td class="td-name">
                                            <h5>{{$item->product->name}}</h5>
                                        </td>

                                        <td class="td-number">
                                            <small>$</small>{{$item->product->price}}
                                        </td>
                                        <td class="td-number">
                                            <button class="btn btn-success change-quantity "
                                                    data-quantity="-1"
                                                    data-product_id="{{$item->product->id}}"
                                                    data-action="{{route('cart.change_quantity')}}"
                                                    style="margin-right: 10px;width: 30px!important; height: 30px!important;">
                                                <span class="text-white">-</span></button>
                                            <span class="span-number"
                                                  id="quantity{{$item->id}}">{{$item->quantity}}</span>

                                            <button class="btn btn-success change-quantity"
                                                    data-quantity="+1"
                                                    data-product_id="{{$item->product->id}}"
                                                    data-action="{{route('cart.change_quantity')}}"
                                                    style="margin-left: 10px;width: 30px!important; height: 30px!important;">
                                                <span class="text-white">+</span></button>
                                        </td>
                                        <td class="td-number">
                                            <small>$</small><span class="notification"
                                                                  id="total{{$item->id}}">{{$item->total}}</span>
                                        </td>

                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-lg-4 mt-5 cart-wrap ftco-animate fadeInUp ftco-animated">
                        <div class="cart-total mb-3">
                            <h3>Coupon Code</h3>
                            <p>Enter your coupon code if you have one</p>
                            <form action="{{route('orders.add_coupon')}}" method="post" class="info">
                                @csrf
                                <div class="form-group">
                                    <label for="">Coupon code</label>
                                    <input type="text" class="form-control text-left px-3 text-uppercase" name="code"
                                           type="text" placeholder="CODE"
                                    >
                                </div>
                                <p>
                                    <button type="submit" class="btn btn-primary py-3 px-4 "
                                            style="color: #fff!important;">
                                        Apply Coupon
                                    </button>
                                </p>

                            </form>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-5 cart-wrap ftco-animate fadeInUp ftco-animated">

                    </div>
                    <div class="col-lg-4 mt-5 cart-wrap ftco-animate fadeInUp ftco-animated">
                        <div class="cart-total mb-3">
                            <h3>Cart Totals</h3>

                            <p class="d-flex total-price">
                                <span>Total</span>
                                <span>${{$carts->sum('total')}}</span>
                            </p>
                        </div>
                        <p><a href="{{route('orders.index')}}" class="btn btn-primary py-3 px-4">Proceed to Checkout</a>
                        </p>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection


