@extends('user.layout.app')
@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('{{asset('user/asset/images/bg_1.jpg')}}');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Home</a></span> <span>Your Profile</span>
                    </p>
                    <h1 class="mb-0 bread">Profile</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section contact-section bg-light">
        <div class="container">
            <div class="row d-flex justify-content-center ">
                <div class="card">
                    <div class=" d-flex justify-content-around " style="margin-top: 20px; padding: 0 20px;">
                        <div class="card border-0 mb-3" >
                            <img src="{{asset('upload/'.auth()->user() ->image)}}" alt="..."
                                 style="width: 400px; height: 400px; border-radius: 1%;margin-right: 50px!important;">
                        </div>
                        <div class="card border-success mb-3" style="width: 400px;">
                            <div class="card-header text-center">Profile</div>
                            <div class="card-body ">
                                <p class="card-text"><span class="text-success">Email: </span>{{auth()->user() ->email}}</p>
                                <p class="card-text"> <span class="text-success">Name: </span> {{auth()->user() ->name}}</p>
                                <p class="card-text"><span class="text-success">Phone: </span> {{auth()->user() ->phone}}</p>
                                <p class="card-text"><span class="text-success">Address: </span>  {{auth()->user() ->address}}</p>
                                <p class="card-text"><span class="text-success">Gender: </span>
                                    @if(auth()->user() ->gender == 1)
                                        Male
                                    @else
                                        Female
                                    @endif

                                </p>
                                <a href="{{route('profile.edit', auth()->user() ->id)}}" class="btn btn-success">Edit</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
