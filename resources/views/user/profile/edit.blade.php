@extends('user.layout.app')
@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('{{asset('user/asset/images/bg_1.jpg')}}');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Home</a></span> <span>Your Profile</span>
                    </p>
                    <h1 class="mb-0 bread">Profile</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section contact-section bg-light">
        <div class="container">
            <div class="row d-flex justify-content-center ">
                <div class="card">
                    <div class=" d-flex justify-content-around " style="margin-top: 20px; padding: 0 20px;">
                        <div class="card border-0 mb-3">
                            <img src="{{asset('upload/'.auth()->user() ->image)}}" alt="..."
                                 style="width: 400px; height: 400px; border-radius: 1%;margin-right: 50px!important;">
                        </div>
                        <div class="card border-success mb-3" style="width: 400px;">
                            <div class="card-header text-center">Profile</div>
                            <div class="card-body ">
                                <form method="POST" action="{{ route('clients.update', $user->id) }}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <p class="card-text">
                                        <span class="text-success">Email: </span>
                                        <input id="email" type="email" class="form-control @error('email')
                                            is-invalid @enderror" name="email" value="{{ $user->email }}"
                                               autocomplete="email"
                                               autofocus required>
                                    </p>
                                    <p class="card-text"><span
                                            class="text-success">Name: </span>
                                        <input id="name" type="text" class="form-control @error('name')
                                            is-invalid @enderror" name="name" value="{{ $user->name }}"
                                               autocomplete="name"
                                               autofocus required></p>
                                    <p class="card-text"><span
                                            class="text-success">Phone: </span>
                                        <input id="phone" type="text" class="form-control @error('phone')
                                            is-invalid @enderror" name="phone" value="{{ $user->phone }}"
                                               autocomplete="phone"
                                               autofocus required></p>
                                    <p class="card-text"><span
                                            class="text-success">Address: </span>
                                        <input id="address" type="text" class="form-control @error('address')
                                            is-invalid @enderror" name="address" value="{{ $user->address }}"
                                               autocomplete="address"
                                               autofocus required></p>
                                    <p class="card-text"><span class="text-success">Gender: </span>
                                        @if(auth()->user() ->gender == 1)
                                            Male
                                        @else
                                            Female
                                        @endif

                                    </p>
                                    <button type="submit" class="btn btn-success">Edit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
