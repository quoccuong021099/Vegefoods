@extends('user.layout.app')

@section('content')

    <!-- Breadcrumb Section Begin -->
    <div class="hero-wrap hero-bread" style="background-image: url('{{asset('user/asset/images/bg_1.jpg')}}'); margin-bottom: 80px">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center fadeInUp ftco-animated">
                    <p class="breadcrumbs"><span class="mr-2"><a href="#">Order Information</a></span>
                    <h1 class="mb-0 bread">Order</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section End -->

    <!-- Checkout Section Begin -->
    @if(session('message'))

        <div class="row">
            <div class="alert alert-warning alert-dismissible fade show w-100" role="alert">
                <strong>{{session('message')}} </strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

    @endif

    <div class="container" style="margin-bottom: 80px">
        @if($orders->count() < 1)
            <h2 class="text-center text-primary" style="margin: 300px 0px;">You have nothing</h2>

        @else
            <div class="row">
                @foreach($orders as $item)
                <div class="card" style="margin: 0px 10px!important; padding-top: 20px">
                            <div class="col-12">
                                <h6 class="text-success text-center">Order Info</h6>
                                <p>Recipient's Name: {{$item->customer}}</p>
                                <p>Address: {{$item->address}}</p>
                                <p>Payment methods: {{$item->payment}}</p>
                                <h6 class="text-success text-center">Order List</h6>
                                @foreach($item->orderDetails as $detail)
                                    <p>N.o: {{$loop->iteration}} - Name: {{$detail->product->name}} - Quantity: {{$detail->quantity}}</p>
                                @endforeach
                        </div>
                </div>
                @endforeach
            </div>

    @endif

    <!-- Checkout Section End -->
    {{--    <div>--}}
    {{--        {{ $orders->links() }}--}}
    {{--    </div>--}}
@endsection
