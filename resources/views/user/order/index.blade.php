@extends('user.layout.app')

@section('content')

    @if(session('message'))
        <div class="container">
            <div class="row">
                <div class="alert alert-warning alert-dismissible fade show w-100" role="alert">
                    <strong>{{session('message')}} </strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    @endif
    @if($carts->count() <1)
        <h2 class="text-center text-primary" style="margin: 300px 0px;">You have nothing</h2>
    @else
        <div class="hero-wrap hero-bread" style="background-image: url('{{asset('user/asset/images/bg_1.jpg')}}');">
            <div class="container">
                <div class="row no-gutters slider-text align-items-center justify-content-center">
                    <div class="col-md-9 ftco-animate text-center fadeInUp ftco-animated">
                        <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Home</a></span>
                            <span>Checkout</span></p>
                        <h1 class="mb-0 bread">Checkout</h1>
                    </div>
                </div>
            </div>
        </div>
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-7 ftco-animate fadeInUp ftco-animated">
                        <form action="{{route('orders.store')}}" class="billing-form" method="post">
                            @csrf
                            <h3 class="mb-4 billing-heading">Billing Details</h3>
                            <div class="row align-items-end">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="firstname">FullName</label>
                                        <input type="text" class="form-control" name="customer" placeholder="fullname"
                                               required>
                                    </div>
                                </div>

                                <div class="w-100"></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="streetaddress">Street Address</label>
                                        <input type="text" class="form-control" name="address"
                                               placeholder="House number and street name" required>
                                    </div>
                                </div>
                                <div class="w-100"></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="text" name="phone" class="form-control"
                                               placeholder="your phone number" required>
                                    </div>
                                </div>
                                <div class="w-100"></div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="emailaddress">Email Address</label>
                                        <input type="text" class="form-control" placeholder="your email" required>
                                    </div>
                                </div>

                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-12">
                                <div class="cart-detail p-3 p-md-4">
                                    <h3 class="billing-heading mb-4">Payment Method</h3>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="radio">
                                                <label><input type="radio" name="payment" class="mr-2" value="tienmat"> Payment on
                                                    delivery</label>
                                                <span class="circle"> </span>
                                            <span class="check"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="radio">
                                                <label><input type="radio" name="payment" class="mr-2" value="card" checked> Payment by card</label>
                                                <span class="circle"> </span>
                                            <span class="check"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <p><button type="submit" class="btn btn-success">Place an order</button></p>
                                </div>
                            </div>
                        </form><!-- END -->
                    </div>
                    <div class="col-xl-5">
                        <div class="row mt-5 pt-3">
                            <div class="col-md-12 d-flex mb-5">
                                <div class="cart-detail cart-total p-3 p-md-4">
                                    <h3 class="billing-heading mb-4">Cart Total</h3>
                                    <p class="d-flex">
                                        <span>Subtotal</span>
                                        <span>${{$carts->sum('total')}}</span>
                                    </p>
                                    <p class="d-flex">
                                        <span>Discount</span>
                                        <span>{{ session()->has('discount') ?'$'. session('discount') :'' }}</span>
                                    </p>
                                    <hr>
                                    <hr>
                                    <p class="d-flex total-price">
                                        <span>Total</span>
                                        <span>@if(session()->has('totalAfterDiscount'))
                                                {{  session('totalAfterDiscount')}}
                                            @else
                                                {{$carts->sum('total')}}
                                            @endif</span>
                                    </p>
                                    <p>{{ session()->has('code') ?'You have applied the code: '. session('code') : '' }}</p>

                                </div>
                            </div>
                        </div>
                    </div> <!-- .col-md-8 -->
                </div>
            </div>
        </section>
    @endif

@endsection
