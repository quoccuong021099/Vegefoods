@extends('manager.layout.app')

@section('title', 'Edit Coupons')

@section('content')
    <a class="btn btn-rose " href="{{route('coupon.index')}}">
      <span class="material-icons text-white">
        arrow_back
      </span>
    </a>
    <div class="row justify-content-center">
        <div class="col-md-12 ">
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <span class="material-icons">
                            aspect_ratio
                            </span>
                    </div>
                    <h4 class="card-title">Edit Coupon</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('coupon.update', $coupon->id) }}"   >
                        @csrf
                        @method('put')

                        <div class="form-group row">
                            <label for="code" class="col-md-4 col-form-label">{{ __('code') }}</label>

                            <div class="col-md-6">
                                <input id="code" type="text" class="form-control @error('code') is-invalid @enderror" name="code" value="{{ $coupon->code }}"  autocomplete="code" autofocus required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="expiry_date" class="col-md-4 col-form-label">{{ __('expiry_date') }}</label>

                            <div class="col-md-6">
                                <input id="expiry_date" type="text" class="form-control @error('expiry_date') is-invalid @enderror" name="expiry_date" value="{{ $coupon->expiry_date }}"  autocomplete="expiry_date" autofocus required>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="quantity" class="col-md-4 col-form-label">{{ __('quantity') }}</label>

                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control @error('quantity') is-invalid @enderror" name="quantity" value="{{ $coupon->quantity }}"  autocomplete="quantity" autofocus required>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label">{{ __('type') }}</label>

                            <div class="col-md-6">
                                <input id="type" type="text" class="form-control @error('type') is-invalid @enderror" name="type" value="{{ $coupon->type }}"  autocomplete="type" autofocus required>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="value" class="col-md-4 col-form-label">{{ __('value') }}</label>

                            <div class="col-md-6">
                                <input id="value" type="text" class="form-control @error('value') is-invalid @enderror" name="value" value="{{ $coupon->value }}"  autocomplete="value" autofocus required>
                            </div>
                        </div>




                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-rose">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
