@extends('manager.layout.app')

@section('title', 'Coupon')

@section('content')
    <h1 class="text-center" style="color: #e91e63; margin-bottom: 50px;  font-weight: 400;">LIST COUPON</h1>
    <div class="col-md-12">
        <a class="btn btn-rose" href="{{route('coupon.create')}}"><span class="material-icons">
            add_circle_outline 
            </span> Create Coupon</a>
        <div class="card">
          <div class="card-header card-header-rose card-header-icon">
            <div class="card-icon">
              <i class="material-icons">assignment</i>
            </div>
            <h4 class="card-title">Coupon Table</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th class="text-center">N.o</th>
                    <th>Code</th>
                    <th>Quantity</th>
                    <th>Type</th>
                    <th>Value</th>
                    <th class="text-right">Expery Date</th>
                    <th class="text-right">Actions</th>
                  </tr>
                </thead>
                <tbody>
            @foreach($coupons as $coupon)

    
                  <tr>
                    <td class="text-center">{{$loop->iteration}}</td>
                    <td>{{$coupon->code}}</td>
                    <td>{{$coupon->quantity}}</td>
                    <td>{{$coupon->type}}</td>
                    <td>{{$coupon->value}}</td>
                    <td class="td-actions text-right">{{$coupon->expiry_date}}</td>
                    <td class="td-actions text-right">
                    <a class="btn btn-success" href="{{route('coupon.edit', $coupon->id)}}"><i class="material-icons">edit</i></a>
                    <form action="{{route('coupon.destroy', $coupon->id)}}"  method="post" style="display: inline">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger " type="submit"style="background-color: #e91e63"><i class="material-icons">close</i></button>
    
                    </form>
                    </td>
                  </tr>
            @endforeach
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    <script>

        @if(session('message'))
        alert("{{session('message')}}");
        @endif
    </script>
    <div>
        {{ $coupons->links() }}
    </div>

@endsection
