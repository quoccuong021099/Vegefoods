@extends('manager.layout.app')

@section('title', 'Create Coupons')

@section('content')
    <a class="btn btn-rose " href="{{route('coupon.index')}}">
      <span class="material-icons text-white">
        arrow_back
      </span>
    </a>
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                    <span class="material-icons">
                        aspect_ratio
                        </span>
                </div>
                <h4 class="card-title">Create Coupon</h4>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('coupon.store') }}"  >
                    @csrf


                    <div class="form-group row">
                        <div class="col-md-6">
                            <input id="code" type="text" placeholder="{{ __('code') }}" class="form-control @error('code') is-invalid @enderror" name="code" value="{{ old('code') }}"  autocomplete="code" autofocus required>


                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <select class="selectpicker" data-live-search="true" name="type"  data-style="select-with-transition" required>
                                <option value="">Choose Type </option>

                                    <option value="Money">Money</option>
                                    <option value="Percent">Percent</option>
                            </select>


                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-6">
                            <input id="value" type="number" placeholder="{{ __('value') }}" class="form-control @error('value') is-invalid @enderror" name="value" value="{{ old('value') }}"  autocomplete="value" autofocus required>

                        </div>
                    </div>
                   <div class="form-group row">
                        <div class="col-md-6">
                            <input id="quantity" type="number" placeholder="{{ __('quantity') }}" class="form-control @error('quantity') is-invalid @enderror" name="quantity" value="{{ old('quantity') }}"  autocomplete="quantity" autofocus required>


                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <input id="expiry_date" type="date" placeholder="{{ __('expiry_date') }}" class="form-control @error('expiry_date') is-invalid @enderror" name="expiry_date" value="{{ old('expiry_date') }}"  autocomplete="expery_date" autofocus required>

                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-rose">
                                {{ __('Create Coupon') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
