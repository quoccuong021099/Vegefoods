<div class="sidebar" data-color="rose" data-background-color="black" data-image="../assets/img/sidebar-1.jpg">

    <div class="logo"><a href="#" class="simple-text logo-mini">

        </a>
        <a href="{{route('home')}}" class="simple-text logo-normal">
            Vegefoods
        </a></div>
    <div class="sidebar-wrapper">

        <ul class="nav">

            <li class="nav-item active ">
                <a class="nav-link" href="{{route('dashboard.index')}}">
                    <i class="material-icons">dashboard</i>
                    <p> Dashboard </p>
                </a>
            </li>
{{--            @hasrole('manage-user-in-admin')--}}

            <li class="nav-item  ">
                <a class="nav-link" href="{{route('users.index')}}">
                    <i class="material-icons">person</i>
                    <p> User </p>
                </a>
            </li>
{{--            @endhasrole--}}

            <li class="nav-item ">
                <a class="nav-link" href="{{route('category.index')}}">
                    <i class="material-icons">content_paste</i>
                    <p>Category</p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{route('product.index')}}">
                    <i class="material-icons">
                        post_add</i>
                    <p>Products</p>
                </a>
            </li>

            <li class="nav-item ">
                <a class="nav-link" href="{{route('coupon.index')}}">
                    <i class="material-icons">aspect_ratio</i>
                    <p>Coupon</p>
                </a>
            </li>

{{--            <li class="nav-item ">--}}
{{--                <a class="nav-link" href="{{route('roles.index')}}">--}}
{{--                    <i class="material-icons">people_alt</i>--}}
{{--                    <p>Role</p>--}}
{{--                </a>--}}

{{--            </li>--}}
            <li class="nav-item ">
                <a class="nav-link" href="{{route('manager.orders.index')}}">
                    <i class="material-icons">shopping_basket</i>
                    <p>Order</p>
                </a>
            </li>


        </ul>
    </div>
    <div class="sidebar-background"></div>
</div>
