@extends('manager.layout.app')

@section('title', 'Products')

@section('content')



    <h1 class="text-center" style="color: #e91e63; margin-bottom: 50px; ; font-weight: 400;">LIST PRODUCTS</h1>
    <div class="col-md-12">
        <form action="{{route('product.index')}}" method="get" class="w-100">
            <div class="row">
                <div class="col-3">
                    <input type="text" placeholder="Name search" class="form-control" value="{{request('name')}}" name="name" >
                </div>
                <div class="col-3">
                    <select name="category_id" class="selectpicker" data-style="select-with-transition"  >
                        <option value="">Category</option>
                        @foreach($categories as $item)
                            <option value="{{$item->id}}"  {{$item->id == request('category_id') ?'selected' : ''}} >{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-3">
                    <button class="btn btn-rose" type="submit"> search</button>
                </div>
            </div>
        </form>
        <a class="btn btn-rose" href="{{route('product.create')}}"><span class="material-icons">
            add_circle_outline 
            </span> Create Product</a>
        <div class="card">
          <div class="card-header card-header-rose card-header-icon">
            <div class="card-icon"> 
              <i class="material-icons">assignment</i>
            </div>
            <h4 class="card-title">Product Table</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th class="text-center">N.o</th>
                    <th>Name</th>
                    <th>Category Name</th>
                    <th>Sale</th>
                    <th>Price</th>
                    <th class="text-right">Image</th>
                    <th class="text-right">Actions</th>
                  </tr>
                </thead>
                <tbody>
            @foreach($products as $product)
                  <tr>
                    <td class="text-center">{{$loop->iteration}}</td>
                    <td>{{$product->name}}</td>
                    <td>
                        @if($product->categories->count() > 0)
                        @foreach($product->categories as $category)
                            {{$category->name}} <br>
                        @endforeach
                    @else
                        <h3>There is no category </h3>
                    @endif    
                    </td>
                    <td>{{$product->sale}}</td>
                    <td> {{$product->price}}</td>
                    <td class="td-actions text-right">
                        <img width="100px" height="100px" src="{{asset('upload/'.$product->image)}}" alt="Anh">
                    </td>
                    <td class="td-actions text-right">
                    <a class="btn btn-success" href="{{route('product.edit', $product->id)}}"><i class="material-icons">edit</i></a>
                    <form action="{{route('product.destroy', $product->id)}}"  method="post" style="display: inline">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger " type="submit"style="background-color: #e91e63"><i class="material-icons">close</i></button>
    
                    </form>
                    </td>
                  </tr>
            @endforeach
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    





    <script>

        @if(session('message'))
        alert("{{session('message')}}");
        @endif
    </script>
    <div>
        {{ $products->links() }}
    </div>

@endsection
