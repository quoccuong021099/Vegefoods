@extends('manager.layout.app')

@section('title', 'Edit Products')

@section('content')
    <a class="btn btn-rose "href="{{route('product.index')}}">
      <span class="material-icons text-white">
        arrow_back
      </span>
    </a>
    <div class="row justify-content-center">
        <div class="col-md-12 ">
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <span class="material-icons">
                            inventory_2
                            </span>
                    </div>
                    <h4 class="card-title">Edit Product</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('product.update', $product->id) }}"  enctype="multipart/form-data">
                        @csrf
                        @method('put')


                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="{{asset('upload/'.$product->image)}}" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                              <span class="btn btn-rose btn-round btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="image">
                                              </span>
                                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                    </div>
                                </div>
                            </div>
                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>




                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $product->name }}"  autocomplete="name" autofocus required>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sale" class="col-md-4 col-form-label">{{ __('sale') }}</label>

                            <div class="col-md-6">
                                <input id="sale" type="text" class="form-control @error('sale') is-invalid @enderror" name="sale" value="{{ $product->sale }}"  autocomplete="sale" autofocus required>

                            </div>
                        </div>





                        <div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label">{{ __('price') }}</label>

                            <div class="col-md-6">
                                <input id="price" type="text" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ $product->price }}"  autocomplete="price" autofocus>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('Categories') }}</label>
                            <div class="col-md-6">
                                <select class="selectpicker" data-live-search="true" name="listCategoryIds[]" multiple data-style="select-with-transition" >
                                    @foreach($categories as $item)
                                        <option value="{{$item->id}}"   {{ in_array($item->id, $listCategoryIds) ? 'selected' : ''  }}    >{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-rose">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
