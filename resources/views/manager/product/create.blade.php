@extends('manager.layout.app')

@section('title', 'Create Products')

@section('content')
    <a class="btn btn-rose " href="{{route('product.index')}}">
      <span class="material-icons text-white">
        arrow_back
      </span>
    </a>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <span class="material-icons">
                            inventory_2
                            </span>
                    </div>
                    <h4 class="card-title">Create Product</h4>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('product.store') }}"  enctype="multipart/form-data">
                        @csrf


                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <h4 class="title">Regular Image</h4>
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="{{asset('manager/asset/img/image_placeholder.jpg')}}" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                              <span class="btn btn-rose btn-round btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="image">
                                              </span>
                                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="name" type="text" placeholder="{{ __('name') }}" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="sale" type="text" placeholder="{{ __('sale') }}" class="form-control @error('sale') is-invalid @enderror" name="sale" value="{{ old('sale') }}"  autocomplete="sale" autofocus required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('Categories') }}</label>

                            <div class="col-md-6">
                                <select class="selectpicker" data-live-search="true" name="category_ids[]" multiple
                                        title="choose categories"
                                        data-style="select-with-transition" >
                                    @foreach($categories as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="price" type="text" placeholder="{{ __('price') }}" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}"  autocomplete="price" autofocus required>

                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-rose">
                                    {{ __('Create Product') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
