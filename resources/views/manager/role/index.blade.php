@extends('manager.layout.app')

@section('title', 'Role')

@section('content')
{{--    <a class="btn btn-primary" href="{{route('roles.create')}}">Create Role</a>--}}

    <h1 class="text-center" style="color: #e91e63; margin-bottom: 50px; font-weight: 400;">LIST ROLE </h1>

    <table  class="table table-bordered text-center">
        <tr>
            <td>STT</td>
            <td>name</td>
            <td>display name</td>
{{--            <td>action</td>--}}
        </tr>

        @foreach($roles as $item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->display_name}}</td>

{{--                <td>--}}
{{--                    <a class="btn btn-warning text-white" href="{{route('roles.edit', $item->id)}}">Edit</a>--}}

{{--                    <form action="{{route('roles.destroy', $item->id)}}"  method="post" style="display: inline">--}}
{{--                        @csrf--}}
{{--                        @method('DELETE')--}}
{{--                        <button class="btn " style="background-color: #e91e63" type="submit">DELETE</button>--}}

{{--                    </form>--}}
{{--                </td>--}}
            </tr>
        @endforeach
    </table>


    <div>
        {{ $roles->links() }}
    </div>

@endsection
