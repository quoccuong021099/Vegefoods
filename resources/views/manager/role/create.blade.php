@extends('manager.layout.app')

@section('title', 'Create Role')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create Role</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('roles.store') }}"  >
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="name" type="text" placeholder="{{ __('name') }}" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus required>

                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="display_name" type="text" placeholder="{{ __('display_name') }}" class="form-control @error('display_name') is-invalid @enderror" name="display_name" value="{{ old('display_name') }}"  autocomplete="display_name" autofocus required>

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create Role') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
