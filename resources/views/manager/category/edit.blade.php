@extends('manager.layout.app')

@section('title', 'Edit Categories')

@section('content')
    <a class="btn btn-rose " href="{{route('category.index')}}">
      <span class="material-icons text-white">
        arrow_back
      </span>
    </a>
    <div class="row justify-content-center">
        <div class="col-md-12 ">
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">category</i>
                    </div>
                    <h4 class="card-title">Edit Category</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('category.update', $category->id) }}"   >
                        @csrf
                        @method('put')

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $category->name }}"  autocomplete="name" autofocus required>
                            </div>
                        </div>




                        <div class="col-md-6">
                            <select class="selectpicker" name="parent_id" data-live-search="true" name="" data-style="select-with-transition" >
                                @foreach($categories as $item)
                                    <option value="{{$item->id}}"  {{$item->id == $category->parent_id ? 'selected' : ''  }}> {{$item->name}}</option>
                                @endforeach
                            </select>

                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-rose">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
