@extends('manager.layout.app')

@section('title', 'Create Categories')

@section('content')
    <a class="btn btn-rose " href="{{route('category.index')}}">
      <span class="material-icons text-white">
        arrow_back
      </span>
    </a>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">category</i>
                    </div>
                    <h4 class="card-title">Create Category</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('category.store') }}"  >
                        @csrf


                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="name" type="text" placeholder="{{ __('name') }}" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-6">
                                <select class="selectpicker" data-live-search="true" name="parent_id"  data-style="select-with-transition" >
                                    <option value="">Choose Category Parent</option>

                                    @foreach($categories as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>

                                @error('parent_id')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-rose">
                                    {{ __('Create Category') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
