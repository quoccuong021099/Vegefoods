@extends('manager.layout.app')

@section('title', 'Category')

@section('content')


<h1 class="text-center" style="color: #e91e63; margin-bottom: 50px; font-weight: 400;">LIST CATEGORY</h1>

<div class="col-md-12">
    <a class="btn btn-rose" href="{{route('category.create')}}"><span class="material-icons">
        add_circle_outline 
        </span> Create Category</a>
    <div class="card">
      <div class="card-header card-header-rose card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assignment</i>
        </div>
        <h4 class="card-title">Category Table</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th class="text-center">N.o</th>
                <th>Name</th>
                <th>Parent Name</th>
                <th class="text-right">Actions</th>
              </tr>
            </thead>
            <tbody>
        @foreach($categories as $category)
              <tr>
                <td class="text-center">{{$loop->iteration}}</td>
                <td>{{$category->name}}</td>
                <td>{{optional($category->parentCategories)->name ?? 'Not Parent'}}</td>
                <td class="td-actions text-right">
                <a class="btn btn-success" href="{{route('category.edit', $category->id)}}"><i class="material-icons">edit</i></a>
                <form action="{{route('category.destroy', $category->id)}}"  method="post" style="display: inline">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger " type="submit"style="background-color: #e91e63"><i class="material-icons">close</i></button>

                </form>
                </td>
              </tr>
        @endforeach
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

    <script>

        @if(session('message'))
        alert("{{session('message')}}");
        @endif
    </script>
    <div>
        {{ $categories->links() }}
    </div>

@endsection
