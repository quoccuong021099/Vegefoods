@extends('manager.layout.app')

@section('title', 'Create Users')

@section('content')
    <a class="btn btn-rose " href="{{route('users.index')}}">
      <span class="material-icons text-white">
        arrow_back
      </span>
    </a>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">person</i>
                    </div>
                    <h4 class="card-title">Create User</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('users.store') }}"  enctype="multipart/form-data" >
                        @csrf

                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <h4 class="title">User Image</h4>
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="../../assets/img/image_placeholder.jpg" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                              <span class="btn btn-rose btn-round btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="image">
                                              </span>
                                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">

                            <div class="col-md-6">
                                <input id="name" type="text" placeholder="{{ __('name') }}" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>

                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="address" type="text" placeholder="address" class="form-control @error('address') is-invalid @enderror" name="address"  autocomplete="address" autofocus required>
                                    {{ old('address') }}
                                </input>
                            </div>
                        </div>

{{--                        <div class="form-group row">--}}
{{--                            <label for="name" class="col-md-4 col-form-label">{{ __('Roles') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <select class="selectpicker" data-live-search="true" name="roles[]" multiple data-style="select-with-transition" >--}}
{{--                                    @foreach($roles as $item)--}}
{{--                                        <option value="{{$item->id}}">{{$item->display_name}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}

{{--                            </div>--}}
{{--                        </div>--}}


                        <div class="form-group row">

                            <div class="col-md-6">
                                <input id="phone" placeholder="{{ __('phone') }}" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}"  autocomplete="phone" autofocus required>

                            </div>
                        </div>

                        <div class="form-group col-6">
                            <select class="form-control selectpicker" data-style="btn btn-link"   id="gender" name ="gender">
                                    <option value="1">Male</option>
                                    <option value="0">FeMale</option>
                            </select>
                        </div>


                        <div class="form-group row">

                            <div class="col-md-6">
                                <input id="email" type="email"  placeholder="{{ __('E-Mail Address') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" required>

                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-6">
                                <input id="password" placeholder="{{ __(' Password') }}" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password" required>

                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-6">
                                <input id="password-confirm"  placeholder="{{ __('Confirm Password') }}" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-rose">
                                    {{ __('Create User') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
