@extends('manager.layout.app')

@section('title', 'orders')

@section('content')
    <h1 class="text-center" style="color: #e91e63; margin-bottom: 50px;  font-weight: 400;">LIST ORDER</h1>

    <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-rose card-header-icon">
            <div class="card-icon">
              <i class="material-icons">assignment</i>
            </div>
            <h4 class="card-title">Order Table</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th class="text-center">N.o</th>
                    <th>Customer Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Payment</th>
                    <th>Status</th>
                    <th>Total</th>
                    <th>Discount</th>
                    <th class="text-right">Date</th>
                    <th class="text-right">Actions</th>
                  </tr>
                </thead>
                <tbody>
            @foreach($orders as $item)

    
                  <tr>
                    <td class="text-center">{{$loop->iteration}}</td>
                    <td>{{$item->customer}}</td>
                    <td>{{$item->address}}</td>
                    <td>{{$item->phone}}</td>
                    <td>{{$item->payment}}</td>
                    <td>{{$item->status}}</td>
                    <td>{{$item->total}}</td>
                    <td>{{$item->discount}}</td>
                    <td class="td-actions text-right">{{$item->order_create}}</td>
                    <td class="td-actions text-right">
                        <a class="btn btn-round btn-just-icon btn-info" href="{{route('manager.orders.show', $item->id)}}">
                            <i class="material-icons">visibility</i>
    
                        </a>
                    </td>
                  </tr>
            @endforeach
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    
 

              

    <div>
        {{ $orders   ->links() }}
    </div>

@endsection
