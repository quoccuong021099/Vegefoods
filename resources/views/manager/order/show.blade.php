@extends('manager.layout.app')

@section('title', 'orders information')

@section('content')


  <a class="btn btn-rose " href="{{route('manager.orders.index')}}">
    <span class="material-icons text-white">
      arrow_back
    </span>
  </a>
<h1 class="text-center" style="color: #e91e63; margin-bottom: 50px; font-weight: 400;">INFORMATION ORDER</h1>
<div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-rose card-header-icon">
        <div class="card-icon">
          <i class="material-icons">assignment</i>
        </div>
        <h4 class="card-title">Information Order Table</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th class="text-center">N.o</th>
                <th>Customer Name</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
                @foreach($order->orderDetails as $item)
                <tr>
                    <td class="text-center">{{$loop->iteration}}</td>
                    <td>{{$order->customer}}</td>
                    <td>{{$item->product->name}}</td>
                    <td>{{$item->quantity}}</td>
                    <td >{{$order->total}}</td>
                    
                </tr>
                  
                @endforeach

            </tbody>
          </table>
          
        </div>
      </div>
    </div>
    <div class="col-lg-6">
    <select name="status" class="selectpicker" data-style="select-with-transition" >
      @foreach(config('data.order_status') as $key => $status)
          <option value="{{$key}}"  {{$order->status == $key ?'selected' : ''}} >{{$status}}</option>
      @endforeach
    </select>
    <button class="btn btn-rose">Update Order Status</button>
    </div>
  </div> 



@endsection
