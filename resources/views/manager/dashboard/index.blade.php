@extends('manager.layout.app')

@section('title', 'Dashboard')

@section('content')



        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">inventory_2
                                </i>
                            </div>
                            <p class="card-category">Products</p>
                            <h3 class="card-title">{{$products}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons text-success">inventory_2</i>
                                <a href="{{route('product.index')}}">Manage Product</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-danger card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">account_circle</i>
                            </div>
                            <p class="card-category">Users</p>
                            <h3 class="card-title">{{$user}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons text-danger">account_circle</i>
                                <a href="{{route('users.index')}}">Manage User</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">category
                                </i>
                            </div>
                            <p class="card-category">Categories</p>
                            <h3 class="card-title">{{$categories}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons text-info">category</i>
                                <a href="{{route('category.index')}}">Manage Categories</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">local_offer
                                </i>
                            </div>
                            <p class="card-category">Coupons</p>
                            <h3 class="card-title">{{$coupons}}
                            </h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons text-warning">local_offer</i>
                                <a href="{{route('coupon.index')}}">Manage Coupons</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">shopping_basket
                                </i>
                            </div>
                            <p class="card-category">Orders</p>
                            <h3 class="card-title">{{$orders}}
                            </h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons text-primary">shopping_basket</i>
                                <a href="{{route('orders.index')}}">Manage Orders</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
@endsection
