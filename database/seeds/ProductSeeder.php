<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Product::create([
            'name' =>'Hoa hong do'
        ]);
        \App\Models\Product::create([
            'name' =>'Hoa hong vang'
        ]);
        \App\Models\Product::create([
            'name' =>'Hoa hong trang'
        ]);
        \App\Models\Product::create([
            'name' =>'Hoa  loa ken'
        ]);
        \App\Models\Product::create([
            'name' =>'Hoa thach thao'
        ]);



        \App\Models\Category::create([
            'name'=> 'Hoa hong'
        ]);
        \App\Models\Category::create([
            'name'=> 'Hoa khac'
        ]);
        \App\Models\Category::create([
            'name'=> 'quan sale'
        ]);

    }
}
