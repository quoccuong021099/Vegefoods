<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('customer');
            $table->string('address');
            $table->string('phone');
            $table->string('payment');
            $table->string('ship');
            $table->unsignedBigInteger('coupon_id')->nullable();
            $table->string('status');
            $table->float('total');
            $table->float('discount');
            $table->timestamp('order_create');
            $table->timestamps();
        });


        Schema::create('order_detail', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('product_id');
            $table->string('quantity');
            $table->string('size');
            $table->string('color');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('order_detail');
        Schema::dropIfExists('orders');
    }

}
