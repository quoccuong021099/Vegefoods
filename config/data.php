<?php
return [
    'order_status' =>[
        'tiep-nhan-don-hang' => 'Receiving orders',
        'dang-xu-ly-don-hang' => 'Processing an order',
        'giao-cho-don-vi-van-chuyen' => 'Delivered to the carrier',
        'dang-van-chuyen' => 'Being shipped',
        'than-cong' => 'Shipping is successful',
        'khach-hang-huy-hang' => 'Cancel',
        'shop-het-hang' => 'Out of stock',
    ]
];
